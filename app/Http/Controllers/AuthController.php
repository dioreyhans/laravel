<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }
    public function welcome(Request $request){
        //dd($request->all());
        return view('welcome',[
            "fname" => $request['fname'],
            'lname' => $request['lname'],
            'jenis' => $request['nationality']
        ]);
    }
}
